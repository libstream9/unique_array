#ifndef STREAM9_UNIQUE_ARRAY_IPP
#define STREAM9_UNIQUE_ARRAY_IPP

#include <algorithm>

#include <stream9/errors.hpp>
#include <stream9/iterators.hpp>
#include <stream9/json.hpp>

namespace stream9 {

template<typename Base>
class unique_array_iterator
    : public iterators::iterator_facade<unique_array_iterator<Base>,
                                std::random_access_iterator_tag,
                                decltype(*std::declval<Base>().begin()->get()) >
{
    using base_t = decltype(std::declval<Base>().begin());

public:
    unique_array_iterator() = default;

    unique_array_iterator(base_t const base) noexcept
        : m_base { base }
    {}

    unique_array_iterator(
        unique_array_iterator<std::remove_const_t<Base>> const& other)
        noexcept
        requires std::is_const_v<Base>
        : m_base { other.base() }
    {}

    unique_array_iterator(unique_array_iterator const&) = default;
    unique_array_iterator& operator=(unique_array_iterator const&) = default;

    unique_array_iterator(unique_array_iterator&&) = default;
    unique_array_iterator& operator=(unique_array_iterator&&) = default;

    base_t base() const noexcept
    {
        return m_base;
    }

    operator base_t () const noexcept
    {
        return m_base;
    }

private:
    friend class iterators::iterator_core_access;

    decltype(auto)
    dereference() const noexcept
    {
        return *m_base->get();
    }

    void increment() noexcept
    {
        ++m_base;
    }

    void decrement() noexcept
    {
        --m_base;
    }

    std::ptrdiff_t
    distance_to(unique_array_iterator const other) const noexcept
    {
        return other.m_base - m_base;
    }

    void
    advance(std::ptrdiff_t const n) noexcept
    {
        m_base += n;
    }

    bool equal(unique_array_iterator const other) const noexcept
    {
        return m_base == other.m_base;
    }

    std::strong_ordering
    compare(unique_array_iterator const other) const noexcept
    {
        return m_base <=> other.m_base;
    }

private:
    base_t m_base {};
};

template<typename T, typename A>
unique_array<T, A>::
unique_array() noexcept = default;

template<typename T, typename A>
unique_array<T, A>::
unique_array(size_type count)
    requires std::is_default_constructible_v<T>
{
    m_arr.reserve(count);

    for (size_type i = 0; i < count; ++i) {
        m_arr.emplace_back();
    }
}

template<typename T, typename A>
unique_array<T, A>::
unique_array(size_type count, T const& value)
    requires std::is_copy_constructible_v<T>
{
    m_arr.reserve(count);

    for (size_type i = 0; i < count; ++i) {
        m_arr.push_back(std::make_unique<T>(value));
    }
}

template<typename T, typename A>
template<std::input_iterator I>
unique_array<T, A>::
unique_array(I first, I const last)
    requires std::is_constructible_v<T, std::iter_reference_t<I>>
{
    if constexpr (std::sized_sentinel_for<I, I>) {
        safe_integer const len = std::distance(first, last);
        m_arr.reserve(len);
    }

    while (first != last) {
        m_arr.push_back(std::make_unique<T>(*first));
        ++first;
    }
}

template<typename T, typename A>
template<typename U>
unique_array<T, A>::
unique_array(std::initializer_list<T> ilist)
    requires std::is_copy_constructible_v<T>
{
    m_arr.reserve(ilist.size());

    for (auto& v: ilist) {
        m_arr.push_back(std::make_unique<T>(std::move(v)));
    }
}

template<typename T, typename A>
unique_array<T, A>& unique_array<T, A>::
operator=(std::initializer_list<T> ilist)
    requires std::is_copy_constructible_v<T>
{
    assign(std::move(ilist));
    return *this;
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
begin() noexcept
{
    return m_arr.begin();
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
end() noexcept
{
    return m_arr.end();
}

template<typename T, typename A>
unique_array<T, A>::const_iterator unique_array<T, A>::
begin() const noexcept
{
    return m_arr.begin();
}

template<typename T, typename A>
unique_array<T, A>::const_iterator unique_array<T, A>::
end() const noexcept
{
    return m_arr.end();
}

template<typename T, typename A>
unique_array<T, A>::reference unique_array<T, A>::
front() noexcept
{
    return *m_arr.front();
}

template<typename T, typename A>
unique_array<T, A>::const_reference unique_array<T, A>::
front() const noexcept
{
    return *m_arr.front();
}

template<typename T, typename A>
unique_array<T, A>::reference unique_array<T, A>::
back() noexcept
{
    return *m_arr.back();
}

template<typename T, typename A>
unique_array<T, A>::const_reference unique_array<T, A>::
back() const noexcept
{
    return *m_arr.back();
}

template<typename T, typename A>
unique_array<T, A>::reference unique_array<T, A>::
at(size_type pos)
{
    try {
        return *m_arr.at(pos);
    }
    catch (...) {
        json::object cxt { { "pos", pos } };

        try {
            throw;
        }
        catch (std::out_of_range const&) {
            throw_error(
                make_error_code(std::errc::argument_out_of_domain),
                std::move(cxt) );
        }
        catch (...) {
            rethrow_error(std::system_category(), std::move(cxt));
        }
    }
}

template<typename T, typename A>
unique_array<T, A>::const_reference unique_array<T, A>::
at(size_type pos) const
{
    try {
        return *m_arr.at(pos);
    }
    catch (...) {
        json::object cxt { { "pos", pos } };

        try {
            throw;
        }
        catch (std::out_of_range const&) {
            throw_error(
                make_error_code(std::errc::argument_out_of_domain),
                std::move(cxt) );
        }
        catch (...) {
            rethrow_error(std::system_category(), std::move(cxt));
        }
    }
}

template<typename T, typename A>
unique_array<T, A>::reference unique_array<T, A>::
operator[](size_type pos) noexcept
{
    return *m_arr[pos];
}

template<typename T, typename A>
unique_array<T, A>::const_reference unique_array<T, A>::
operator[](size_type pos) const noexcept
{
    return *m_arr[pos];
}

template<typename T, typename A>
unique_array<T, A>::size_type unique_array<T, A>::
size() const noexcept
{
    return m_arr.size();
}

template<typename T, typename A>
unique_array<T, A>::size_type unique_array<T, A>::
max_size() const noexcept
{
    return m_arr.max_size();
}

template<typename T, typename A>
unique_array<T, A>::size_type unique_array<T, A>::
capacity() const noexcept
{
    return m_arr.capacity();
}

template<typename T, typename A>
bool unique_array<T, A>::
empty() const noexcept
{
    return m_arr.empty();
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
insert(const_iterator const pos, T const& v)
    requires std::is_copy_constructible_v<T>
{
    return m_arr.insert(pos.base(), std::make_unique<T>(v));
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
insert(const_iterator const pos, T&& v)
    requires std::is_move_constructible_v<T>
{
    return m_arr.insert(pos.base(), std::make_unique<T>(std::move(v)));
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
insert(const_iterator const pos, std::unique_ptr<T> ptr)
{
    return m_arr.insert(pos.base(), std::move(ptr));
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
insert(const_iterator const pos, size_type const count, T const& v)
    requires std::is_copy_constructible_v<T>
{
    if (count == 0) {
        return m_arr.erase(pos, pos);
    }

    auto const idx = pos.base() - m_arr.begin();

    m_arr.reserve(m_arr.size() + count);

    auto first_it = m_arr.insert(m_arr.begin() + idx, std::make_unique<T>(v));
    auto it = first_it;

    for (size_type i = 1; i < count; ++i) {
        it = m_arr.insert(++it, std::make_unique<T>(v));
    }

    return first_it;
}

template<typename T, typename A>
template<std::input_iterator I>
unique_array<T, A>::iterator unique_array<T, A>::
insert(const_iterator const pos, I first, I const last)
    requires std::is_constructible_v<T, std::iter_reference_t<I>>
{
    if (first == last) {
        return m_arr.erase(pos, pos); // remove_const
    }

    auto const idx = pos.base() - m_arr.begin();

    if constexpr (std::sized_sentinel_for<I, I>) {
        safe_integer const len = std::distance(first, last);
        m_arr.reserve(m_arr.size() + len);
    }

    auto first_it = m_arr.insert(m_arr.begin() + idx, std::make_unique<T>(*first));
    auto it = first_it;

    while (++first != last) {
        it = m_arr.insert(++it, std::make_unique<T>(*first));
    }

    return first_it;
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
insert(const_iterator const pos, std::initializer_list<T> ilist)
    requires std::is_copy_constructible_v<T>
{
    if (ilist.size() == 0) {
        return m_arr.erase(pos, pos); // remove_const
    }

    auto const idx = pos.base() - m_arr.begin();

    m_arr.reserve(m_arr.size() + ilist.size());

    auto first = ilist.begin();
    auto const last = ilist.end();

    auto const first_it = m_arr.insert(
                   m_arr.begin() + idx, std::make_unique<T>(std::move(*first)));
    auto it = first_it;

    while (++first != last) {
        it = m_arr.insert(++it, std::make_unique<T>(std::move(*first)));
    }

    return first_it;
}

template<typename T, typename A>
template<typename... Args>
unique_array<T, A>::iterator unique_array<T, A>::
emplace(const_iterator const pos, Args&&... args)
    requires std::is_constructible_v<T, Args...>
{
    return m_arr.insert(
        pos.base(), std::make_unique<T>(std::forward<Args>(args)...));
}

template<typename T, typename A>
unique_array<T, A>::reference unique_array<T, A>::
push_back(T const& v)
    requires std::is_copy_constructible_v<T>
{
    m_arr.push_back(std::make_unique<T>(v));
    return *m_arr.back();
}

template<typename T, typename A>
unique_array<T, A>::reference unique_array<T, A>::
push_back(T&& v)
    requires std::is_move_constructible_v<T>
{
    m_arr.push_back(std::make_unique<T>(std::move(v)));
    return *m_arr.back();
}

template<typename T, typename A>
unique_array<T, A>::reference unique_array<T, A>::
push_back(std::unique_ptr<T> v)
{
    m_arr.push_back(std::move(v));
    return *m_arr.back();
}

template<typename T, typename A>
template<typename U/*= T*/, typename... Args>
U& unique_array<T, A>::
emplace_back(Args&&... args)
    requires std::is_constructible_v<U, Args...>
          && (std::same_as<U, T> ||
              (std::is_polymorphic_v<U> && std::is_base_of_v<T, U>) )
{
    m_arr.push_back(std::make_unique<U>(std::forward<Args>(args)...));
    return static_cast<U&>(*m_arr.back());
}

template<typename T, typename A>
void unique_array<T, A>::
assign(size_type const count, T const& value)
    requires std::is_copy_constructible_v<T>
{
    unique_array tmp(count, value);

    swap(tmp);
}

template<typename T, typename A>
template<std::input_iterator I>
void unique_array<T, A>::
assign(I const first, I const last)
    requires std::is_constructible_v<T, std::iter_reference_t<I>>
{
    unique_array tmp(first, last);

    swap(tmp);
}

template<typename T, typename A>
void unique_array<T, A>::
assign(std::initializer_list<T> ilist)
    requires std::is_copy_constructible_v<T>
{
    unique_array tmp(std::move(ilist));

    swap(tmp);
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
erase(const_iterator const pos)
{
    return m_arr.erase(pos);
}

template<typename T, typename A>
unique_array<T, A>::iterator unique_array<T, A>::
erase(const_iterator const first, const_iterator const last)
{
    return m_arr.erase(first, last);
}

template<typename T, typename A>
template<typename Pred>
unique_array<T, A>::size_type unique_array<T, A>::
erase_if(Pred pred)
{
    auto it = std::remove_if(m_arr.begin(), m_arr.end(),
                             [&](auto& v) { return pred(*v); } );

    auto r = std::distance(it, m_arr.end());

    m_arr.erase(it, m_arr.end());

    return r;
}

template<typename T, typename A>
void unique_array<T, A>::
pop_back() noexcept
{
    m_arr.pop_back();
}

template<typename T, typename A>
void unique_array<T, A>::
clear() noexcept
{
    m_arr.pop_back();
}

template<typename T, typename A>
void unique_array<T, A>::
resize(size_type const count)
    requires std::is_default_constructible_v<T>
{
    if (size() >= count) {
        m_arr.resize(count);
    }
    else {
        m_arr.reserve(count);

        for (auto i = size(); i < count; ++i) {
            m_arr.push_back(std::make_unique<T>());
        }
    }
}

template<typename T, typename A>
void unique_array<T, A>::
resize(size_type const count, value_type const& value)
    requires std::is_copy_constructible_v<T>
{
    if (size() >= count) {
        m_arr.resize(count);
    }
    else {
        m_arr.reserve(count);

        for (auto i = size(); i < count; ++i) {
            m_arr.push_back(std::make_unique<T>(value));
        }
    }
}

template<typename T, typename A>
void unique_array<T, A>::
reserve(size_type const new_cap)
{
    m_arr.reserve(new_cap);
}

template<typename T, typename A>
void unique_array<T, A>::
shrink_to_fit()
{
    m_arr.shrink_to_fit();
}

template<typename T, typename A>
void unique_array<T, A>::
swap(unique_array& other) noexcept
{
    m_arr.swap(other.m_arr);
}

template<typename T, typename A>
bool unique_array<T, A>::
operator==(unique_array const& other)
{
    namespace rng = std::ranges;
    return rng::equal(*this, other);
}

template<typename T, typename A>
auto unique_array<T, A>::
operator<=>(unique_array const& other)
{
    return std::lexicographical_compare_three_way(
        begin(), end(),
        other.begin(), other.end()
    );
}

template<typename T, typename Alloc>
void
swap(unique_array<T, Alloc>& lhs, unique_array<T, Alloc>& rhs) noexcept
{
    lhs.swap(rhs);
}

} // namespace stream9

#endif // STREAM9_UNIQUE_ARRAY_IPP
