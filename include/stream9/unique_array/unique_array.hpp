#ifndef STREAM9_UNIQUE_ARRAY_UNIQUE_ARRAY_HPP
#define STREAM9_UNIQUE_ARRAY_UNIQUE_ARRAY_HPP

#include <cstdint>
#include <initializer_list>
#include <iterator>
#include <memory>
#include <type_traits>
#include <vector>

#include <stream9/safe_integer.hpp>

namespace stream9 {

template<typename> class unique_array_iterator;

template<typename T,
         typename Allocator = std::allocator<T> >
class unique_array
{
    static_assert(!std::is_reference_v<T>);
    static_assert(!std::is_const_v<T>);

    using array_t = std::vector<std::unique_ptr<T>>;
public:
    using iterator = unique_array_iterator<array_t>;
    using const_iterator = unique_array_iterator<array_t const>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using size_type = safe_integer<std::int64_t, 0>;
    using value_type = T;
    using reference = value_type&;
    using const_reference = value_type const&;
    using pointer = std::add_pointer_t<value_type>;
    using const_pointer = std::add_pointer_t<value_type const>;
    using allocator_type = Allocator;
    using difference_type = safe_integer<std::int64_t, 0>;

public:
    // essential
    unique_array() noexcept;

    unique_array(size_type count)
        requires std::is_default_constructible_v<T>;

    unique_array(size_type count, T const& value)
        requires std::is_copy_constructible_v<T>;

    template<std::input_iterator I>
    unique_array(I first, I last)
        requires std::is_constructible_v<T, std::iter_reference_t<I>>;

    template<typename U = void> //XXX workaround for gcc bug
    unique_array(std::initializer_list<T> ilist)
        requires std::is_copy_constructible_v<T>;

    unique_array& operator=(std::initializer_list<T> ilist)
        requires std::is_copy_constructible_v<T>;

    unique_array(unique_array const&) = delete;
    unique_array& operator=(unique_array const&) = delete;

    unique_array(unique_array&&) = default;
    unique_array& operator=(unique_array&&) = default;

    ~unique_array() = default;

    // accessor
    iterator begin() noexcept;
    iterator end() noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    reference front() noexcept;
    const_reference front() const noexcept;

    reference back() noexcept;
    const_reference back() const noexcept;

    reference at(size_type pos);
    const_reference at(size_type pos) const;

    reference operator[](size_type pos) noexcept;
    const_reference operator[](size_type pos) const noexcept;

    //pointer data() noexcept;              // pointless to have this because elements aren't in a contiguous memory.
    //const_pointer data() const noexcept;

    // query
    size_type size() const noexcept;

    size_type max_size() const noexcept;

    size_type capacity() const noexcept;

    bool empty() const noexcept;

    // modifier
    iterator insert(const_iterator pos, T const&)
        requires std::is_copy_constructible_v<T>;

    iterator insert(const_iterator pos, T&&)
        requires std::is_move_constructible_v<T>;

    iterator insert(const_iterator pos, std::unique_ptr<T>);

    iterator insert(const_iterator pos, size_type count, T const&)
        requires std::is_copy_constructible_v<T>;

    template<std::input_iterator I>
    iterator insert(const_iterator pos, I first, I last)
        requires std::is_constructible_v<T, std::iter_reference_t<I>>;

    iterator insert(const_iterator pos, std::initializer_list<T> ilist)
        requires std::is_copy_constructible_v<T>;

    template<typename... Args>
    iterator emplace(const_iterator pos, Args&&... args)
        requires std::is_constructible_v<T, Args...>;

    reference push_back(T const&)
        requires std::is_copy_constructible_v<T>;

    reference push_back(T&&)
        requires std::is_move_constructible_v<T>;

    reference push_back(std::unique_ptr<T>);

    template<typename U = T, typename... Args>
    U&
    emplace_back(Args&&... args)
        requires std::is_constructible_v<U, Args...>
              && (std::same_as<U, T> ||
                  (std::is_polymorphic_v<U> && std::is_base_of_v<T, U>) );

    void assign(size_type count, T const& value)
        requires std::is_copy_constructible_v<T>;

    template<std::input_iterator I>
    void assign(I first, I last)
        requires std::is_constructible_v<T, std::iter_reference_t<I>>;

    void assign(std::initializer_list<T> ilist)
        requires std::is_copy_constructible_v<T>;

    iterator erase(const_iterator pos);
    iterator erase(const_iterator first, const_iterator last);

    template<typename Pred>
    size_type erase_if(Pred pred);

    void pop_back() noexcept;

    void clear() noexcept;

    void resize(size_type count)
        requires std::is_default_constructible_v<T>;

    void resize(size_type count, value_type const& value)
        requires std::is_copy_constructible_v<T>;

    void reserve(size_type new_cap);

    void shrink_to_fit();

    void swap(unique_array& other) noexcept;

    // comparison
    bool operator==(unique_array const& other);
    auto operator<=>(unique_array const& other);

    template<typename T1, typename Alloc>
    friend void
    swap(unique_array<T1, Alloc>& lhs, unique_array<T1, Alloc>& rhs) noexcept;

private:
    array_t m_arr;
};

template<std::input_iterator It,
       typename Alloc = std::pmr::polymorphic_allocator<std::iter_value_t<It>> >
unique_array(It, It, Alloc = Alloc())
    -> unique_array<std::iter_value_t<It>, Alloc>;

} // namespace stream9

#include "unique_array.ipp"

#endif // STREAM9_UNIQUE_ARRAY_UNIQUE_ARRAY_HPP
